<?php

function fieldpage_admin() {
  $form = array();

  $form['titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set titles'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  foreach (_fieldpage_get_entity_types() as $entity_type) {
    $title_config = 'fieldpage_' . $entity_type . '_title_config';
    $entity_info = entity_get_info($entity_type);
    $form['titles'][$title_config] = array(
      '#type'=> 'textfield',
      '#title'=> t('Title of %entity_label entity', array('%entity_label' => $entity_info['label'])),
      '#default_value' => variable_get($title_config),
      '#size' => 50,
      '#maxlength' => 255
    );

    $form['other']['fieldpage_disable_blocks'] = array(
      '#type'=> 'checkbox',
      '#title'=> t('Disable all blocks on fieldpage pages.'),
      '#default_value' => variable_get('fieldpage_disable_all_blocks'),
      '#size' => 10,
      '#maxlength' => 10
    );

  }

  return system_settings_form($form);
}

